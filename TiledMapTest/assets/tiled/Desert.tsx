<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Desert" tilewidth="32" tileheight="32" spacing="1" margin="1">
 <image source="tiled/tmw_desert_spacing.png" width="265" height="199"/>
 <tile id="30">
  <properties>
   <property name="cactus" value="true"/>
  </properties>
 </tile>
 <tile id="39">
  <properties>
   <property name="cactus" value="true"/>
  </properties>
 </tile>
</tileset>
