package com.rods.tcc.handler;

import java.util.ArrayList;

import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.util.constants.Constants;

import com.rods.tcc.character.GameCharacter;
import com.rods.tcc.util.TMXTileUtil;

public class TCCUpdateHandler implements IUpdateHandler {

	private final TMXTiledMap mTMXTiledMap;
	private final Scene scene;
	private final GameCharacter[] characters;
	private final TMXLayer tmxLayer;
	private TMXTile tmpTile = null;
	private TMXTile[] tmpArrayTile = null;

	public TCCUpdateHandler(TMXTiledMap mTMXTiledMap, TMXLayer tmxLayer, Scene scene, GameCharacter... characters) {
		this.mTMXTiledMap = mTMXTiledMap;
		this.tmxLayer = tmxLayer;
		this.scene = scene;
		
		if (TMXTileUtil.DEBUG) {
			for (int i = 0; i < characters.length; i++) {
				characters[i].setRectangleArray(getVisionRectangles(characters[i].getNumberOfTiles(), scene));
			}
		}
		
		this.characters = characters;
	}
	
	@Override
	public void onUpdate(float pSecondsElapsed) {
		for (int i = 0; i < characters.length; i++) {
			GameCharacter npc = characters[i];
			final float[] playerFootCordinates = npc.convertLocalToSceneCoordinates(12, 31);
			
			final TMXTile tmxTile = tmxLayer.getTMXTileAt(playerFootCordinates[Constants.VERTEX_INDEX_X], playerFootCordinates[Constants.VERTEX_INDEX_Y]);
			final TMXTile[] tmxArrayTile = npc.getVisionTiles(tmxLayer, tmxTile);
			
			for (int j = 0; j < tmxArrayTile.length; j++) {
				TMXTile tmxBaseTile = tmxArrayTile[j];
				if (tmxBaseTile != null){
					TMXProperties<TMXTileProperty> tmxTileProperties = tmxBaseTile.getTMXTileProperties(mTMXTiledMap);
					if (tmxTileProperties != null) {
						TMXTileProperty tmxProperty = tmxTileProperties.get(0);
						String prop = tmxProperty.getName();
						String value = tmxProperty.getValue();
						
						if (npc.getOnFindTileWithPropertyListener() != null && tmpArrayTile != null) {
							if (tmpArrayTile[j].getTileColumn() != tmxBaseTile.getTileColumn() ||
									tmpArrayTile[j].getTileRow() != tmxBaseTile.getTileRow() ) {
								npc.getOnFindTileWithPropertyListener().onFindTileWithProperty(prop, value);
							}
						}
						
					}
				}
				if (npc != null && tmxBaseTile != null && TMXTileUtil.DEBUG) {
					ArrayList<Rectangle> rectangleArray = npc.getRectangleArray();
					Rectangle rectangle = rectangleArray.get(j);
					rectangle.setPosition(tmxBaseTile.getTileX(), tmxBaseTile.getTileY());;
				}
			}
			
			if (tmpTile == null) {
				tmpTile = tmxTile;
			} else {
				if (tmpTile.getTileColumn() != tmxTile.getTileColumn() ||
						tmpTile.getTileRow() != tmxTile.getTileRow() ) {
					if (tmpTile.getTileRow() < tmxTile.getTileRow()) {
						npc.setNpcDirection(GameCharacter.IS_GOING_RIGHT);
					} else if (tmpTile.getTileRow() > tmxTile.getTileRow()) {
						npc.setNpcDirection(GameCharacter.IS_GOING_LEFT);
					} else if (tmpTile.getTileColumn() < tmxTile.getTileColumn()) {
						npc.setNpcDirection(GameCharacter.IS_GOING_DOWN);
					} else if (tmpTile.getTileColumn() > tmxTile.getTileColumn()) {
						npc.setNpcDirection(GameCharacter.IS_GOING_UP);
					}
					tmpTile = tmxTile;
					tmpArrayTile = tmxArrayTile;
				}
			}
			
		} 
	}

	@Override
	public void reset() {
		
	}

	private ArrayList<Rectangle> getVisionRectangles(int numberOfRects, Scene scene){
		ArrayList<Rectangle> rects = new ArrayList<Rectangle>();
		for (int i = 0; i < numberOfRects; i++) {
			final Rectangle rect = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
			rect.setColor(1, 0, 0, 0.25f);
			rects.add(rect);
			
			scene.getLastChild().attachChild(rect);
		}
		return rects;
	}
	
}
