package com.rods.tcc.example;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl.IOnScreenControlListener;
import org.anddev.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.anddev.andengine.engine.handler.IUpdateHandler;
import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.Debug;
import org.anddev.andengine.util.constants.Constants;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.rods.tcc.character.MainCharacter;
import com.rods.tcc.listener.OnCharacterStatusChangedListener;
import com.rods.tcc.listener.OnFindTileWithPropertyListener;
import com.rods.tcc.util.TMXTileUtil;

/**
 * @author Nicolas Gramlich
 * @since 13:58:48 - 19.07.2010
 */
public class TMXTiledMapExampleTwo extends BaseExample {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH 	= 480;
	private static final int CAMERA_HEIGHT 	= 320;
	private static final int IS_GOING_DOWN 	= 1;
	private static final int IS_GOING_UP 	= 2;
	private static final int IS_GOING_LEFT 	= 3;
	private static final int IS_GOING_RIGHT = 4;

	// ===========================================================
	// Fields
	// ===========================================================

	private int playerDirection = IS_GOING_DOWN;
	private BoundCamera mBoundChaseCamera;

	private Texture mTexture;
	private TiledTextureRegion mPlayerTextureRegion;
	private TMXTiledMap mTMXTiledMap;
	protected int mCactusCount;

	private Texture mOnScreenControlTexture;
	private TextureRegion mOnScreenControlBaseTextureRegion;
	private TextureRegion mOnScreenControlKnobTextureRegion;

	
	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	public Handler toaster = new Handler(){      
        @Override
        public void handleMessage(Message msg) {
             Toast.makeText(getBaseContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
	};

	
	@Override
	public Engine onLoadEngine() {
		Toast.makeText(this, "The tile the player is walking on will be highlighted.", Toast.LENGTH_LONG).show();
		this.mBoundChaseCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new Engine(new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mBoundChaseCamera));
	}

	@Override
	public void onLoadResources() {
		this.mTexture = new Texture(128, 128, TextureOptions.DEFAULT);
		this.mPlayerTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "player.png", 0, 0, 3, 4); // 72x128
		
		this.mOnScreenControlTexture = new Texture(256, 128, TextureOptions.DEFAULT);
		this.mOnScreenControlBaseTextureRegion = TextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_base.png", 0, 0);
		this.mOnScreenControlKnobTextureRegion = TextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_knob.png", 128, 0);
		
		this.mEngine.getTextureManager().loadTextures(this.mTexture, this.mOnScreenControlTexture);
	}

	@Override
	public Scene onLoadScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());

		final Scene scene = new Scene(2);

		try {
			final TMXLoader tmxLoader = new TMXLoader(this, this.mEngine.getTextureManager(), TextureOptions.BILINEAR_PREMULTIPLYALPHA, new ITMXTilePropertiesListener() {
				@Override
				public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {
					/* We are going to count the tiles that have the property "cactus=true" set. */
					if(pTMXTileProperties.containsTMXProperty("cactus", "true")) {
						TMXTiledMapExampleTwo.this.mCactusCount++;
					}
				}
			});
			this.mTMXTiledMap = tmxLoader.loadFromAsset(this, "tiled/desert.tmx");
			
			Toast.makeText(this, "Cactus count in this TMXTiledMap: " + this.mCactusCount, Toast.LENGTH_LONG).show();
		} catch (final TMXLoadException tmxle) {
			Debug.e(tmxle);
		}

		final TMXLayer tmxLayer = this.mTMXTiledMap.getTMXLayers().get(0);
		scene.getFirstChild().attachChild(tmxLayer);

		/* Make the camera not exceed the bounds of the TMXEntity. */
		this.mBoundChaseCamera.setBounds(0, tmxLayer.getWidth(), 0, tmxLayer.getHeight());
		this.mBoundChaseCamera.setBoundsEnabled(true);

		/* Calculate the coordinates for the face, so its centered on the camera. */
		final int centerX = (CAMERA_WIDTH - this.mPlayerTextureRegion.getTileWidth()) / 2;
		final int centerY = (CAMERA_HEIGHT - this.mPlayerTextureRegion.getTileHeight()) / 2;

		/* Create the sprite and add it to the scene. */
		final MainCharacter player = new MainCharacter(centerX, centerY, this.mPlayerTextureRegion);
		final PhysicsHandler physicsHandler = new PhysicsHandler(player);
		player.registerUpdateHandler(physicsHandler);
		player.setOnCharacterStatusChangedListener(new OnCharacterStatusChangedListener() {
			@Override
			public void onCharacterStatusChanged(int newState, boolean restartWhenIsDone) {
				switch(newState) {
				case MainCharacter.STATE_GOING_DOWN:
					player.animate(new long[]{200, 200, 200}, 6, 8, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_RIGHT:
					player.animate(new long[]{200, 200, 200}, 3, 5, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_UP:
					player.animate(new long[]{200, 200, 200}, 0, 2, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_LEFT:
					player.animate(new long[]{200, 200, 200}, 9, 11, restartWhenIsDone);
					break;
				}
			}
		});
		
		this.mBoundChaseCamera.setChaseEntity(player);

//		final Path path = new Path(5).to(0, 160).to(0, 500).to(600, 500).to(600, 160).to(0, 160);
//		player.registerEntityModifier(new LoopEntityModifier(new PathModifier(30, path, null, new IPathModifierListener() {
//			@Override
//			public void onWaypointPassed(final PathModifier pPathModifier, final IEntity pEntity, final int pWaypointIndex) {
//				switch(pWaypointIndex) {
//					case 0:
//						player.animate(new long[]{200, 200, 200}, 6, 8, true);
//						break;
//					case 1:
//						player.animate(new long[]{200, 200, 200}, 3, 5, true);
//						break;
//					case 2:
//						player.animate(new long[]{200, 200, 200}, 0, 2, true);
//						break;
//					case 3:
//						player.animate(new long[]{200, 200, 200}, 9, 11, true);
//						break;
//				}
//			}
//		})));

		final DigitalOnScreenControl analogOnScreenControl = new DigitalOnScreenControl(0, CAMERA_HEIGHT - this.mOnScreenControlBaseTextureRegion.getHeight(), this.mBoundChaseCamera, this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion, 0.1f, new IOnScreenControlListener() {
			@Override
			public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl,
										final float pValueX, final float pValueY) {
				
				TMXTile tmxTile = player.getNextTile(tmxLayer, pValueX, pValueY);
				
				if (tmxTile != null) {
					physicsHandler.setVelocity(pValueX * 100, pValueY * 100);
					TMXTileUtil.handleTileProperty(mTMXTiledMap, tmxTile, new OnFindTileWithPropertyListener() {
						@Override
						public void onFindTileWithProperty(String property, String name) {
							if (property.equals("sign") && name.equals("stop")) {
								if (pValueY != 0) {
									physicsHandler.setVelocity(pValueX * 100, 0 * 100);
									return;
								} else if (pValueX != 0) {
									physicsHandler.setVelocity(0 * 100, pValueY * 100);
									return;
								}
							}
						}
					});
				}
				
			}
		});
		
		analogOnScreenControl.getControlBase().setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		analogOnScreenControl.getControlBase().setAlpha(0.3f);
		analogOnScreenControl.getControlBase().setScaleCenter(0, 128);
		analogOnScreenControl.getControlBase().setScale(1.25f);
		analogOnScreenControl.getControlKnob().setScale(1.25f);
		analogOnScreenControl.refreshControlKnobPosition();
		
		scene.setChildScene(analogOnScreenControl);

		/* Now we are going to create a rectangle that will  always highlight the tile below the feet of the pEntity. */
		final Rectangle currentTileRectangle = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
		currentTileRectangle.setColor(1, 0, 0, 0.25f);
		scene.getLastChild().attachChild(currentTileRectangle);

		//VISION RECTANGLE
		final Rectangle visionTileRectangleBase = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
		visionTileRectangleBase.setColor(1, 0, 0, 0.25f);
		final Rectangle visionTileRectangle1 = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
		visionTileRectangle1.setColor(0, 0, 1, 0.25f);
		final Rectangle visionTileRectangle2 = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
		visionTileRectangle2.setColor(0, 1, 0, 0.25f);
		final Rectangle visionTileRectangle3 = new Rectangle(0, 0, this.mTMXTiledMap.getTileWidth(), this.mTMXTiledMap.getTileHeight());
		visionTileRectangle3.setColor(1, 1, 0, 0.25f);
		
		scene.getLastChild().attachChild(visionTileRectangleBase);
		scene.getLastChild().attachChild(visionTileRectangle1);
		scene.getLastChild().attachChild(visionTileRectangle2);
		scene.getLastChild().attachChild(visionTileRectangle3);
		
		final Rectangle[] rectArray = {visionTileRectangleBase, visionTileRectangle1, visionTileRectangle2, visionTileRectangle3};
		
		scene.registerUpdateHandler(new IUpdateHandler() {
			
			TMXTile tmpTile = null;
			
			@Override
			public void reset() { }

			@Override
			public void onUpdate(final float pSecondsElapsed) {
				/* Get the scene-coordinates of the players feet. */
				final float[] playerFootCordinates = player.convertLocalToSceneCoordinates(12, 31);

				//TODO VISION FIELD - CONSTRUTOR PARA TIPO DE VISAO
				/* Get the tile the feet of the player are currently waking on. */
				final TMXTile tmxTile = tmxLayer.getTMXTileAt(playerFootCordinates[Constants.VERTEX_INDEX_X], playerFootCordinates[Constants.VERTEX_INDEX_Y]);

				//TODO VISION FIELD
				final TMXTile[] tmxArrayTile = getVisionTile(playerDirection, tmxLayer, tmxTile);
//				final TMXTile tmxTileBase = tmxLayer.getTMXTile(tmxTile.getTileColumn(), pTileRow)
				// Usar tmxLayer.getTMXTile - passando o numero da coluna e da linha que deseja obter oTIle
				// dessa forma � possivel pegar todos os tiles necess�rios e construir o campo de vis�o
				
				if (tmxArrayTile[3] != null){
					TMXProperties<TMXTileProperty> tmxTileProperties = tmxArrayTile[3].getTMXTileProperties(mTMXTiledMap);
					if (tmxTileProperties != null) {
						TMXTileProperty tmxProperty = tmxTileProperties.get(0);
						String prop = tmxProperty.getName();
						String value = tmxProperty.getValue();
						
						if (prop.equals("sign") && value.equals("stop")) {
							if (tmpTile.getTileColumn() != tmxArrayTile[3].getTileColumn() ||
									tmpTile.getTileRow() != tmxArrayTile[3].getTileRow() ) {
								makeToast("A placa da c�lula amarela diz \"PARE!\"");
//							Log.e("","cactus == true");
							}
						}
					}
				}
				
				if(tmxTile != null) {
					// tmxTile.setTextureRegion(null); <-- Rubber-style removing of tiles =D
					currentTileRectangle.setPosition(tmxTile.getTileX(), tmxTile.getTileY());
				}
				
				for (int i = 0 ; i < 4 ; i++) {
					TMXTile tmxBaseTile = tmxArrayTile[i];
					if (tmxBaseTile != null) {
						rectArray[i].setPosition(tmxBaseTile.getTileX(), tmxBaseTile.getTileY());
					}
				}
				
				
				if (tmpTile == null) {
					tmpTile = tmxTile;
				} else {
					if (tmpTile.getTileColumn() != tmxTile.getTileColumn() ||
							tmpTile.getTileRow() != tmxTile.getTileRow() ) {
						if (tmpTile.getTileRow() < tmxTile.getTileRow()) {
							playerDirection = IS_GOING_RIGHT;
						} else if (tmpTile.getTileRow() > tmxTile.getTileRow()) {
							playerDirection = IS_GOING_LEFT;
						} else if (tmpTile.getTileColumn() < tmxTile.getTileColumn()) {
							playerDirection = IS_GOING_DOWN;
						} else if (tmpTile.getTileColumn() > tmxTile.getTileColumn()) {
							playerDirection = IS_GOING_UP;
						}
						tmpTile = tmxTile;
					}
				}
			}
		});
		scene.getLastChild().attachChild(player);
		
		return scene;
	}

	private TMXTile[] getVisionTile(int playerDirection, TMXLayer tmxLayer, TMXTile tmxTile) {
		TMXTile[] tiles = {null,null,null,null};
		int columnModifier = 0;
		int rowModifier = 0;
		
		int columnModifier1 = 0;
		int rowModifier1 = 0;
		int columnModifier2 = 0;
		int rowModifier2 = 0;
		int columnModifier3 = 0;
		int rowModifier3 = 0;
		
		if (playerDirection == IS_GOING_UP) {
			columnModifier = -1;
			//TODO PROBLEMA AO IR PARA CIMA
			columnModifier1 = -1;
			columnModifier2 = -1;
			columnModifier3 = -1;
			rowModifier1 = -1;
			rowModifier2 = 0;
			rowModifier3 = 1;
		} else if (playerDirection == IS_GOING_DOWN) {
			columnModifier = 1;
			
			columnModifier1 = 1;
			columnModifier2 = 1;
			columnModifier3 = 1;
			rowModifier1 = 1;
			rowModifier2 = 0;
			rowModifier3 = -1;
		} else if (playerDirection == IS_GOING_LEFT) {
			rowModifier = -1;
			
			rowModifier1 = -1;
			rowModifier2 = -1;
			rowModifier3 = -1;
			columnModifier1 = 1;
			columnModifier1 = 0;
			columnModifier1 = -1;
		} else if (playerDirection == IS_GOING_RIGHT) {
			rowModifier = 1;
			
			rowModifier1 = 1;
			rowModifier2 = 1;
			rowModifier3 = 1;
			columnModifier1 = -1;
			columnModifier1 = 0;
			columnModifier1 = 1;
		}

		int pTileColumn = tmxTile.getTileColumn() + columnModifier;
		int pTileRow = tmxTile.getTileRow() + rowModifier;
		setTileArray(0, tmxLayer, tiles, pTileColumn, pTileRow);
		setTileArray(1, tmxLayer, tiles, pTileColumn + columnModifier1, pTileRow + rowModifier1);
		setTileArray(2, tmxLayer, tiles, pTileColumn + columnModifier2, pTileRow + rowModifier2);
		setTileArray(3, tmxLayer, tiles, pTileColumn + columnModifier3, pTileRow + rowModifier3);
		
		return tiles;
	}

	private void setTileArray(int pos, TMXLayer tmxLayer, TMXTile[] tiles,
			int pTileColumn, int pTileRow) {
		if (pTileColumn < 0 || pTileRow < 0) {
			tiles[pos] = null;
		} else {
			tiles[pos] = tmxLayer.getTMXTile(pTileColumn, pTileRow);
		}
	}
	
	public void makeToast(final String str){
        Message status = toaster.obtainMessage();
        Bundle datax = new Bundle();
        datax.putString("msg", str);
        status.setData(datax);
        toaster.sendMessage(status);
	}

	
	@Override
	public void onLoadComplete() {

	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
