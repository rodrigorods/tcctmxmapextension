package com.rods.tcc.example;

import javax.microedition.khronos.opengles.GL10;

import org.anddev.andengine.engine.Engine;
import org.anddev.andengine.engine.camera.BoundCamera;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl;
import org.anddev.andengine.engine.camera.hud.controls.BaseOnScreenControl.IOnScreenControlListener;
import org.anddev.andengine.engine.camera.hud.controls.DigitalOnScreenControl;
import org.anddev.andengine.engine.handler.physics.PhysicsHandler;
import org.anddev.andengine.engine.options.EngineOptions;
import org.anddev.andengine.engine.options.EngineOptions.ScreenOrientation;
import org.anddev.andengine.engine.options.resolutionpolicy.RatioResolutionPolicy;
import org.anddev.andengine.entity.IEntity;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXLoader.ITMXTilePropertiesListener;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;
import org.anddev.andengine.entity.layer.tiled.tmx.util.exception.TMXLoadException;
import org.anddev.andengine.entity.modifier.LoopEntityModifier;
import org.anddev.andengine.entity.modifier.PathModifier;
import org.anddev.andengine.entity.modifier.PathModifier.IPathModifierListener;
import org.anddev.andengine.entity.modifier.PathModifier.Path;
import org.anddev.andengine.entity.scene.Scene;
import org.anddev.andengine.entity.util.FPSLogger;
import org.anddev.andengine.opengl.texture.Texture;
import org.anddev.andengine.opengl.texture.TextureOptions;
import org.anddev.andengine.opengl.texture.region.TextureRegion;
import org.anddev.andengine.opengl.texture.region.TextureRegionFactory;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.Debug;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.rods.tcc.character.GameCharacter;
import com.rods.tcc.character.MainCharacter;
import com.rods.tcc.handler.TCCUpdateHandler;
import com.rods.tcc.listener.OnCharacterStatusChangedListener;
import com.rods.tcc.listener.OnFindTileWithPropertyListener;
import com.rods.tcc.util.TMXTileUtil;

/**
 * @author Nicolas Gramlich
 * @since 13:58:48 - 19.07.2010
 */
public class TMXTiledMapExample extends BaseExample {
	// ===========================================================
	// Constants
	// ===========================================================

	private static final int CAMERA_WIDTH 	= 480;
	private static final int CAMERA_HEIGHT 	= 320;

	// ===========================================================
	// Fields
	// ===========================================================

	private BoundCamera mBoundChaseCamera;

	private Texture mTexture;
	private TiledTextureRegion mPlayerTextureRegion;
	private TiledTextureRegion mNPCTextureRegion;
	private TMXTiledMap mTMXTiledMap;
	protected int mCactusCount;

	private Texture mOnScreenControlTexture;
	private TextureRegion mOnScreenControlBaseTextureRegion;
	private TextureRegion mOnScreenControlKnobTextureRegion;

	
	// ===========================================================
	// Constructors
	// ===========================================================

	// ===========================================================
	// Getter & Setter
	// ===========================================================

	// ===========================================================
	// Methods for/from SuperClass/Interfaces
	// ===========================================================

	public Handler toaster = new Handler(){      
        @Override
        public void handleMessage(Message msg) {
             Toast.makeText(getBaseContext(), msg.getData().getString("msg"), Toast.LENGTH_SHORT).show();
        }
	};

	
	@Override
	public Engine onLoadEngine() {
		Toast.makeText(this, "The tile the player is walking on will be highlighted.", Toast.LENGTH_LONG).show();
		this.mBoundChaseCamera = new BoundCamera(0, 0, CAMERA_WIDTH, CAMERA_HEIGHT);
		return new Engine(new EngineOptions(true, ScreenOrientation.LANDSCAPE, new RatioResolutionPolicy(CAMERA_WIDTH, CAMERA_HEIGHT), this.mBoundChaseCamera));
	}

	@Override
	public void onLoadResources() {
		this.mTexture = new Texture(128, 128, TextureOptions.DEFAULT);
		this.mPlayerTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "player.png", 0, 0, 3, 4); // 72x128
		this.mNPCTextureRegion = TextureRegionFactory.createTiledFromAsset(this.mTexture, this, "player.png", 0, 0, 3, 4); // 72x128
		
		this.mOnScreenControlTexture = new Texture(256, 128, TextureOptions.DEFAULT);
		this.mOnScreenControlBaseTextureRegion = TextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_base.png", 0, 0);
		this.mOnScreenControlKnobTextureRegion = TextureRegionFactory.createFromAsset(this.mOnScreenControlTexture, this, "onscreen_control_knob.png", 128, 0);
		
		this.mEngine.getTextureManager().loadTextures(this.mTexture, this.mOnScreenControlTexture);
	}

	@Override
	public Scene onLoadScene() {
		this.mEngine.registerUpdateHandler(new FPSLogger());

		final Scene scene = new Scene(2);

		try {
			final TMXLoader tmxLoader = new TMXLoader(this, this.mEngine.getTextureManager(), TextureOptions.BILINEAR_PREMULTIPLYALPHA, new ITMXTilePropertiesListener() {
				@Override
				public void onTMXTileWithPropertiesCreated(final TMXTiledMap pTMXTiledMap, final TMXLayer pTMXLayer, final TMXTile pTMXTile, final TMXProperties<TMXTileProperty> pTMXTileProperties) {
					/* We are going to count the tiles that have the property "cactus=true" set. */
					if(pTMXTileProperties.containsTMXProperty("cactus", "true")) {
						TMXTiledMapExample.this.mCactusCount++;
					}
				}
			});
			this.mTMXTiledMap = tmxLoader.loadFromAsset(this, "tiled/desert.tmx");
			
			Toast.makeText(this, "Cactus count in this TMXTiledMap: " + this.mCactusCount, Toast.LENGTH_LONG).show();
		} catch (final TMXLoadException tmxle) {
			Debug.e(tmxle);
		}

		final TMXLayer tmxLayer = this.mTMXTiledMap.getTMXLayers().get(0);
		scene.getFirstChild().attachChild(tmxLayer);

		/* Make the camera not exceed the bounds of the TMXEntity. */
		this.mBoundChaseCamera.setBounds(0, tmxLayer.getWidth(), 0, tmxLayer.getHeight());
		this.mBoundChaseCamera.setBoundsEnabled(true);

		/* Calculate the coordinates for the face, so its centered on the camera. */
		final int centerX = (CAMERA_WIDTH - this.mPlayerTextureRegion.getTileWidth()) / 2;
		final int centerY = (CAMERA_HEIGHT - this.mPlayerTextureRegion.getTileHeight()) / 2;

		/* Create the sprite and add it to the scene. */
		final MainCharacter player = new MainCharacter(centerX + 10, centerY, this.mPlayerTextureRegion);
		final PhysicsHandler physicsHandler = new PhysicsHandler(player);
		player.registerUpdateHandler(physicsHandler);
		player.setOnCharacterStatusChangedListener(new OnCharacterStatusChangedListener() {
			@Override
			public void onCharacterStatusChanged(int newState, boolean restartWhenIsDone) {
				switch(newState) {
				case MainCharacter.STATE_GOING_DOWN:
					player.animate(new long[]{200, 200, 200}, 6, 8, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_RIGHT:
					player.animate(new long[]{200, 200, 200}, 3, 5, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_UP:
					player.animate(new long[]{200, 200, 200}, 0, 2, restartWhenIsDone);
					break;
				case MainCharacter.STATE_GOING_LEFT:
					player.animate(new long[]{200, 200, 200}, 9, 11, restartWhenIsDone);
					break;
				}
			}
		});
		
		this.mBoundChaseCamera.setChaseEntity(player);

		final DigitalOnScreenControl analogOnScreenControl = new DigitalOnScreenControl(0, 
				CAMERA_HEIGHT - this.mOnScreenControlBaseTextureRegion.getHeight(), this.mBoundChaseCamera, 
				this.mOnScreenControlBaseTextureRegion, this.mOnScreenControlKnobTextureRegion,
				0.1f, new IOnScreenControlListener() {
			
			@Override
			public void onControlChange(final BaseOnScreenControl pBaseOnScreenControl,
										final float pValueX, final float pValueY) {
				
				TMXTile tmxTile = player.getNextTile(tmxLayer, pValueX, pValueY);
				
				if (tmxTile != null) {
					physicsHandler.setVelocity(pValueX * 100, pValueY * 100);
					TMXTileUtil.handleTileProperty(mTMXTiledMap, tmxTile, new OnFindTileWithPropertyListener() {
						@Override
						public void onFindTileWithProperty(String property, String name) {
							if (property.equals("sign") && name.equals("stop")) {
								if (pValueY != 0) {
									physicsHandler.setVelocity(pValueX * 100, 0 * 100);
									return;
								} else if (pValueX != 0) {
									physicsHandler.setVelocity(0 * 100, pValueY * 100);
									return;
								}
							}
						}
					});
				}
				
			}
		});
		
		analogOnScreenControl.getControlBase().setBlendFunction(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		analogOnScreenControl.getControlBase().setAlpha(0.3f);
		analogOnScreenControl.getControlBase().setScaleCenter(0, 128);
		analogOnScreenControl.getControlBase().setScale(1.25f);
		analogOnScreenControl.getControlKnob().setScale(1.25f);
		analogOnScreenControl.refreshControlKnobPosition();
		
		scene.setChildScene(analogOnScreenControl);

		final GameCharacter npc = new GameCharacter(centerX, centerY, this.mNPCTextureRegion, GameCharacter.VISION_AROUND);
		
		final Path path = new Path(5).to(0, 160).to(0, 500).to(600, 500).to(600, 160).to(0, 160);
		npc.registerEntityModifier(new LoopEntityModifier(new PathModifier(30, path, null, new IPathModifierListener() {
			@Override
			public void onWaypointPassed(final PathModifier pPathModifier, final IEntity pEntity, final int pWaypointIndex) {
				switch(pWaypointIndex) {
					case 0:
						npc.animate(new long[]{200, 200, 200}, 6, 8, true);
						break;
					case 1:
						npc.animate(new long[]{200, 200, 200}, 3, 5, true);
						break;
					case 2:
						npc.animate(new long[]{200, 200, 200}, 0, 2, true);
						break;
					case 3:
						npc.animate(new long[]{200, 200, 200}, 9, 11, true);
						break;
				}
			}
		})));
		
		npc.setOnFindTileWithPropertyListener(new OnFindTileWithPropertyListener() {
			@Override
			public void onFindTileWithProperty(String property, String name) {
			if (property.equals("sign") && name.equals("stop")) {
					Log.e("","A placa diz \"PARE!\"");
				}
			}
		});
		
		TCCUpdateHandler updateHandler = new TCCUpdateHandler(mTMXTiledMap, tmxLayer, scene, npc);
		scene.registerUpdateHandler(updateHandler);
		
		scene.getLastChild().attachChild(player);
		scene.getLastChild().attachChild(npc);
		
		return scene;
	}

	public void makeToast(final String str){
        Message status = toaster.obtainMessage();
        Bundle datax = new Bundle();
        datax.putString("msg", str);
        status.setData(datax);
        toaster.sendMessage(status);
	}

	
	@Override
	public void onLoadComplete() {

	}

	// ===========================================================
	// Methods
	// ===========================================================

	// ===========================================================
	// Inner and Anonymous Classes
	// ===========================================================
}
