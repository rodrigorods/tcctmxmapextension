package com.rods.tcc.util;

import org.anddev.andengine.entity.layer.tiled.tmx.TMXProperties;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTileProperty;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTiledMap;

import com.rods.tcc.listener.OnFindTileWithPropertyListener;


public class TMXTileUtil {

	public static final int correctionColisionTile = 10;
	public static final boolean DEBUG = true;
	
	public static void handleTileProperty(TMXTiledMap mTMXTiledMap, TMXTile tmxTile, OnFindTileWithPropertyListener... onFindTileWithPropertyListener) {
		TMXProperties<TMXTileProperty> tmxTileProperties = tmxTile.getTMXTileProperties(mTMXTiledMap);
		if (tmxTileProperties != null) {
			TMXTileProperty tmxProperty = tmxTileProperties.get(0);
			String prop = tmxProperty.getName();
			String value = tmxProperty.getValue();

			for (int i = 0 ; i < onFindTileWithPropertyListener.length ; i++) {
				onFindTileWithPropertyListener[i].onFindTileWithProperty(prop, value);
			}
		}
	}
}
