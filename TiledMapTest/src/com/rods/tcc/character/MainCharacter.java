package com.rods.tcc.character;

import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;
import org.anddev.andengine.util.constants.Constants;

import com.rods.tcc.listener.OnCharacterStatusChangedListener;
import com.rods.tcc.util.TMXTileUtil;

public class MainCharacter extends AnimatedSprite {

	public static final int STATE_NONE = 0;
	public static final int STATE_GOING_UP = 1;
	public static final int STATE_GOING_DOWN = 2;
	public static final int STATE_GOING_LEFT = 3;
	public static final int STATE_GOING_RIGHT = 4;
	public static final int STATE_ATTACKING = 5;
	
	public enum FACTION {NEUTRAL,ALLY,ENEMY};
	
	private int state = STATE_NONE;
	
	private OnCharacterStatusChangedListener onCharacterStatusChangedListener = null;
	
	public MainCharacter(float pX, float pY,
			TiledTextureRegion pTiledTextureRegion) {
		super(pX, pY, pTiledTextureRegion);
	}

	public TMXTile getNextTile(TMXLayer tmxLayer, final float pValueX, final float pValueY) {
		int tmpState = state;
		float[] playerFootCordinates = convertLocalToSceneCoordinates(12, 31);
		TMXTile tmxTile = tmxLayer.getTMXTileAt(
				playerFootCordinates[Constants.VERTEX_INDEX_X],
				playerFootCordinates[Constants.VERTEX_INDEX_Y]);
		if (pValueX < 0) {
			setState(STATE_GOING_LEFT, true);
			tmxTile = tmxLayer.getTMXTileAt(
					playerFootCordinates[Constants.VERTEX_INDEX_X] - TMXTileUtil.correctionColisionTile,
					playerFootCordinates[Constants.VERTEX_INDEX_Y]);
		} else if (pValueX > 0) {
			setState(STATE_GOING_RIGHT, true);
			tmxTile = tmxLayer.getTMXTileAt(
					playerFootCordinates[Constants.VERTEX_INDEX_X] + TMXTileUtil.correctionColisionTile,
					playerFootCordinates[Constants.VERTEX_INDEX_Y]);
		} else if (pValueY > 0) {
			setState(STATE_GOING_DOWN, true);
			tmxTile = tmxLayer.getTMXTileAt(
					playerFootCordinates[Constants.VERTEX_INDEX_X], 
					playerFootCordinates[Constants.VERTEX_INDEX_Y] + TMXTileUtil.correctionColisionTile);
		} else if (pValueY < 0) {
			setState(STATE_GOING_UP, true);
			tmxTile = tmxLayer.getTMXTileAt(
					playerFootCordinates[Constants.VERTEX_INDEX_X], 
					playerFootCordinates[Constants.VERTEX_INDEX_Y] - TMXTileUtil.correctionColisionTile);
		} else {
			setState(tmpState, false);
			if (tmpState == STATE_GOING_LEFT) {
				tmxTile = tmxLayer.getTMXTileAt(
						playerFootCordinates[Constants.VERTEX_INDEX_X] - TMXTileUtil.correctionColisionTile, 
						playerFootCordinates[Constants.VERTEX_INDEX_Y]);
			} else if (tmpState == STATE_GOING_RIGHT) {
				tmxTile = tmxLayer.getTMXTileAt(
						playerFootCordinates[Constants.VERTEX_INDEX_X] + TMXTileUtil.correctionColisionTile, 
						playerFootCordinates[Constants.VERTEX_INDEX_Y]);
			} else if (tmpState == STATE_GOING_DOWN) {
				tmxTile = tmxLayer.getTMXTileAt(
						playerFootCordinates[Constants.VERTEX_INDEX_X], 
						playerFootCordinates[Constants.VERTEX_INDEX_Y] + TMXTileUtil.correctionColisionTile);
			} else if (tmpState == STATE_GOING_UP) {
				tmxTile = tmxLayer.getTMXTileAt(
						playerFootCordinates[Constants.VERTEX_INDEX_X], 
						playerFootCordinates[Constants.VERTEX_INDEX_Y] - TMXTileUtil.correctionColisionTile);
			}
		}
		return tmxTile;
	}
	
	public void setState(int newState, boolean restartWhenIsDone) {
		if (restartWhenIsDone) {
			if (this.state != newState) {
				this.state = newState;
				if (this.onCharacterStatusChangedListener != null) {
					this.onCharacterStatusChangedListener.onCharacterStatusChanged(newState, restartWhenIsDone);
				}
			}
		} else {
			if (this.onCharacterStatusChangedListener != null) {
				this.onCharacterStatusChangedListener.onCharacterStatusChanged(newState, restartWhenIsDone);
			}
		}
	}
	
	public int getState() {
		return state;
	}
	
	public void setOnCharacterStatusChangedListener(
			OnCharacterStatusChangedListener onCharacterStatusChangedListener) {
		this.onCharacterStatusChangedListener = onCharacterStatusChangedListener;
	}
	
}
