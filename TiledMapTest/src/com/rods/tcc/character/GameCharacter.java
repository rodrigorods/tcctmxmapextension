package com.rods.tcc.character;

import java.util.ArrayList;

import org.anddev.andengine.entity.layer.tiled.tmx.TMXLayer;
import org.anddev.andengine.entity.layer.tiled.tmx.TMXTile;
import org.anddev.andengine.entity.primitive.Rectangle;
import org.anddev.andengine.entity.sprite.AnimatedSprite;
import org.anddev.andengine.opengl.texture.region.TiledTextureRegion;

import com.rods.tcc.listener.OnFindTileWithPropertyListener;

public class GameCharacter extends AnimatedSprite {

	public static final int VISION_AROUND = 0;
	public static final int VISION_IN_T = 1;
	
	public static final int IS_GOING_DOWN 	= 1;
	public static final int IS_GOING_UP 	= 2;
	public static final int IS_GOING_LEFT 	= 3;
	public static final int IS_GOING_RIGHT 	= 4;

	private int npcDirection = IS_GOING_DOWN;
	
	private int visionType = -1;
	private ArrayList<Rectangle> rects = null;
	
	private OnFindTileWithPropertyListener onFindTileWithPropertyListener = null;
	
	public GameCharacter(float pX, float pY,
			TiledTextureRegion pTiledTextureRegion, int visionType) {
		super(pX, pY, pTiledTextureRegion);
		this.visionType = visionType;
	}
	
	public int getNumberOfTiles() {
		if (visionType == VISION_AROUND) {
			return 9;
		} else if (visionType == VISION_IN_T) {
			return 5;
		}
		return 0;
	}
	
	public void setRectangleArray(ArrayList<Rectangle> rects) {
		if (this.rects == null){
			this.rects = rects;
		}
	}
	
	public ArrayList<Rectangle> getRectangleArray() {
		return rects;
	}
	
	public TMXTile[] getVisionTiles(TMXLayer tmxLayer, TMXTile tmxTile) {
		if (visionType == VISION_AROUND) {
			return getVisionForAround(tmxLayer,tmxTile);
		} else if (visionType == VISION_IN_T) {
			return getVisionForT(tmxLayer,tmxTile);
		}
		return null;
	}
	
	private TMXTile[] getVisionForT(TMXLayer tmxLayer, TMXTile tmxTile) {
		TMXTile[] tiles = {null,null,null,null};
		int columnModifier = 0;
		int rowModifier = 0;
		
		int columnModifier1 = 0;
		int rowModifier1 = 0;
		int columnModifier2 = 0;
		int rowModifier2 = 0;
		int columnModifier3 = 0;
		int rowModifier3 = 0;
		
		if (npcDirection == IS_GOING_UP) {
			columnModifier = -1;
			columnModifier1 = -1;
			columnModifier2 = -1;
			columnModifier3 = -1;
			rowModifier1 = -1;
			rowModifier2 = 0;
			rowModifier3 = 1;
		} else if (npcDirection == IS_GOING_DOWN) {
			columnModifier = 1;
			
			columnModifier1 = 1;
			columnModifier2 = 1;
			columnModifier3 = 1;
			rowModifier1 = 1;
			rowModifier2 = 0;
			rowModifier3 = -1;
		} else if (npcDirection == IS_GOING_LEFT) {
			rowModifier = -1;
			
			rowModifier1 = -1;
			rowModifier2 = -1;
			rowModifier3 = -1;
			columnModifier1 = 1;
			columnModifier1 = 0;
			columnModifier1 = -1;
		} else if (npcDirection == IS_GOING_RIGHT) {
			rowModifier = 1;
			
			rowModifier1 = 1;
			rowModifier2 = 1;
			rowModifier3 = 1;
			columnModifier1 = -1;
			columnModifier1 = 0;
			columnModifier1 = 1;
		}

		int pTileColumn = tmxTile.getTileColumn() + columnModifier;
		int pTileRow = tmxTile.getTileRow() + rowModifier;
		setTileArray(0, tmxLayer, tiles, pTileColumn, pTileRow);
		setTileArray(1, tmxLayer, tiles, pTileColumn + columnModifier1, pTileRow + rowModifier1);
		setTileArray(2, tmxLayer, tiles, pTileColumn + columnModifier2, pTileRow + rowModifier2);
		setTileArray(3, tmxLayer, tiles, pTileColumn + columnModifier3, pTileRow + rowModifier3);
		
		return tiles;
	}
	
	private TMXTile[] getVisionForAround(TMXLayer tmxLayer, TMXTile tmxTile) {
		TMXTile[] tiles = {null,null,null,null,null,null,null,null,null};
		int pTileColumn = tmxTile.getTileColumn();
		int pTileRow = tmxTile.getTileRow();
		
		setTileArray(0, tmxLayer, tiles, pTileColumn-1, pTileRow-1);
		setTileArray(1, tmxLayer, tiles, pTileColumn-1, pTileRow);
		setTileArray(2, tmxLayer, tiles, pTileColumn-1, pTileRow+1);
		setTileArray(3, tmxLayer, tiles, pTileColumn, pTileRow-1);
		setTileArray(4, tmxLayer, tiles, pTileColumn, pTileRow);
		setTileArray(5, tmxLayer, tiles, pTileColumn, pTileRow+1);
		setTileArray(6, tmxLayer, tiles, pTileColumn+1, pTileRow-1);
		setTileArray(7, tmxLayer, tiles, pTileColumn+1, pTileRow);
		setTileArray(8, tmxLayer, tiles, pTileColumn+1, pTileRow+1);
		
		return tiles;
	}

	private void setTileArray(int pos, TMXLayer tmxLayer, TMXTile[] tiles,
			int pTileColumn, int pTileRow) {
		if (pTileColumn < 0 || pTileRow < 0) {
			tiles[pos] = null;
		} else {
			tiles[pos] = tmxLayer.getTMXTile(pTileColumn, pTileRow);
		}
	}
	
	public int getNpcDirection() {
		return npcDirection;
	}
	
	public void setNpcDirection(int npcDirection) {
		this.npcDirection = npcDirection;
	}
	
	
	public void setOnFindTileWithPropertyListener(
			OnFindTileWithPropertyListener onFindTileWithPropertyListener) {
		this.onFindTileWithPropertyListener = onFindTileWithPropertyListener;
	}
	
	public OnFindTileWithPropertyListener getOnFindTileWithPropertyListener() {
		return onFindTileWithPropertyListener;
	}
	
}
