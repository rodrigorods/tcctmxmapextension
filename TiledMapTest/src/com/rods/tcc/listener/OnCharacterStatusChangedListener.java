package com.rods.tcc.listener;


public interface OnCharacterStatusChangedListener {

	void onCharacterStatusChanged(int newState, boolean restartWhenIsDone);
	
}
