package com.rods.tcc.listener;


public interface OnFindTileWithPropertyListener {

	void onFindTileWithProperty(String property, String name);
	
}
